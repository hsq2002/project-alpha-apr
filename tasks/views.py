from django.shortcuts import render, redirect
from .forms import TaskForm
from django.contrib.auth.decorators import login_required
from .models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            tasks = form.save()
            tasks.owner = request.user
            tasks.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def list_task(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": task,
    }
    return render(request, "tasks/list.html", context)
