from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm

# from .forms import ProjectForm
# Create your views here.


@login_required
def list_projects(request):
    lists = Project.objects.filter(owner=request.user)
    context = {
        "projects_list": lists,
    }
    return render(request, "projects/list.html", context)


@login_required
def project_show(request, id):
    show = get_object_or_404(Project, id=id)
    context = {
        "show_project": show,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid:
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
